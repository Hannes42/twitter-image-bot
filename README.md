# twitter-image-bot

A tiny and simple Python script to post a random image to Twitter. Build for aerial images of Hamburg, Germany posted at https://twitter.com/IrgendwoHH

It simply picks a random image from the configured directory and posts it to Twitter with the configured text. That's all!

![ALT](https://hannes.enjoys.it/blog/wp-content/uploads/image-31.png)

## Installation
- Install pillow and twython (I used Pillow==7.0.0 and twython==3.8.2) in any way you like
- Add your Twitter OAuth credentials in the script
  - Registering a Twitter account and setting it up is a privacy nightmare and, being a good human being as you are, you ought hate any part of it. I followed the [Twython tutorial for OAuth1](https://twython.readthedocs.io/en/latest/usage/starting_out.html#oauth-1-user-authentication) in a live Python interpreter session which was fairly painless. If you do not want to link a phone number to your Twitter account (see above for how you should feel about that), [this approach](https://twittercommunity.com/t/how-to-create-an-app-without-mobile/21135/18) worked for me in the past but I bet they use regional profiling or worse so your luck might differ.

## Usage
- It's just `python post.py`
- Maybe use a cronjob to automate it?

## License
AGPL

## Project status
Abandoned
